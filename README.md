# gradle-plugin-kimber

gradle-plugin-kimber is a gradle plugin for integrating local development gradle builds with kubernetes.  The idea is to get as close as possible to one command being all that is needed to compile the code, build the image, and deploy to the kubernetes cluster.

## Usage

##### build.gradle
```
plugins {
    id "com.sidneysimmons.gradle-plugin-kimber" version "1.0.0"
}
```