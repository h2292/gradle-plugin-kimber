package com.sidneysimmons.kimber.service;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import com.sidneysimmons.kimber.testing.util.TestingUtil;
import java.io.File;
import java.util.Arrays;
import org.gradle.api.Project;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

/**
 * Unit tests for the {@link DockerService} class.
 */
public class DockerServiceTest {

    @InjectMocks
    private DockerService dockerService;

    @Mock
    private ProcessService mockProcessService;

    @BeforeEach
    public void beforeEach() {
        MockitoAnnotations.openMocks(this);
    }

    /**
     * Test the print contexts method.
     */
    @Test
    public void testPrintContexts() {
        dockerService.printContexts();
        verify(mockProcessService, times(1)).executeAndLog(Arrays.asList("docker", "context", "ls"), null);
    }

    /**
     * Test the build image method.
     */
    @Test
    public void testBuildImage() {
        Project mockProject = TestingUtil.mockProject();
        dockerService.buildImage(mockProject);
        verify(mockProcessService, times(1)).executeAndLog(Arrays.asList("docker", "build", "-t", "kimber/" + mockProject.getName() + ":local", "-f",
                mockProject.getProjectDir().getAbsolutePath() + "/Dockerfile", mockProject.getProjectDir().getAbsolutePath()), null);
    }

    /**
     * Test the get docker file method.
     */
    @Test
    public void testGetDockerFile() {
        Project mockProject = TestingUtil.mockProject();
        File file = dockerService.getDockerFile(mockProject);
        assertTrue(file.getAbsolutePath().endsWith("Dockerfile"));
    }

}
