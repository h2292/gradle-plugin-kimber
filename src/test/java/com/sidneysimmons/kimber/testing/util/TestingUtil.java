package com.sidneysimmons.kimber.testing.util;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.File;
import org.gradle.api.Project;

/**
 * Generic testing utility methods.
 */
public class TestingUtil {

    private TestingUtil() {
    }

    /**
     * Mock a {@link Project}.
     * 
     * @return the project
     */
    public static Project mockProject() {
        // Give the project a name
        Project project = mock(Project.class);
        when(project.getName()).thenReturn("Example Project");

        // Give the project a directory
        File projectDirectory = mock(File.class);
        when(projectDirectory.getAbsolutePath()).thenReturn("/project-directory");
        when(project.getProjectDir()).thenReturn(projectDirectory);
        return project;
    }

}
