package com.sidneysimmons.kimber.exception;

/**
 * Exception for when there is a problem executing a process.
 */
public class ProcessExecutionException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public ProcessExecutionException() {
        super();
    }

    public ProcessExecutionException(String message) {
        super(message);
    }

    public ProcessExecutionException(Throwable cause) {
        super(cause);
    }

    public ProcessExecutionException(String message, Throwable cause) {
        super(message, cause);
    }

    public ProcessExecutionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
