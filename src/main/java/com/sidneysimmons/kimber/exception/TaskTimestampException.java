package com.sidneysimmons.kimber.exception;

/**
 * Exception for when there is a problem working with a task timestamp.
 */
public class TaskTimestampException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public TaskTimestampException() {
        super();
    }

    public TaskTimestampException(String message) {
        super(message);
    }

    public TaskTimestampException(Throwable cause) {
        super(cause);
    }

    public TaskTimestampException(String message, Throwable cause) {
        super(message, cause);
    }

    public TaskTimestampException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
