package com.sidneysimmons.kimber.extension;

import org.gradle.api.Project;
import org.gradle.api.provider.Property;

/**
 * Plugin extension for configuring this plugin. Note that this isn't currently being used. Leaving
 * here for now as an example.
 */
public class KimberGradlePluginExtension {

    public static final String NAME = "kimber";
    private Property<String> exampleString;
    private Property<Boolean> exampleBoolean;

    /**
     * Constructor. Create the configurable properties and give them defaults.
     * 
     * @param project the project
     */
    public KimberGradlePluginExtension(Project project) {
        exampleString = project.getObjects().property(String.class);
        exampleString.set("This is a default string.");
        exampleBoolean = project.getObjects().property(Boolean.class);
        exampleBoolean.set(false);
    }

    /**
     * Get the example string.
     * 
     * @return the string
     */
    public Property<String> getExampleString() {
        return exampleString;
    }

    /**
     * Set the example string.
     * 
     * @param exampleString the string to set
     */
    public void setExampleString(Property<String> exampleString) {
        this.exampleString = exampleString;
    }

    /**
     * Get the example boolean.
     * 
     * @return the boolean
     */
    public Property<Boolean> getExampleBoolean() {
        return exampleBoolean;
    }

    /**
     * Set the example boolean.
     * 
     * @param exampleBoolean the boolean to set
     */
    public void setExampleBoolean(Property<Boolean> exampleBoolean) {
        this.exampleBoolean = exampleBoolean;
    }

}