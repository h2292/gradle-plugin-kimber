package com.sidneysimmons.kimber.task;

import com.sidneysimmons.kimber.service.HelmService;
import com.sidneysimmons.kimber.util.FactoryUtil;
import org.gradle.api.DefaultTask;
import org.gradle.api.tasks.TaskAction;

/**
 * Task used to install the helm chart.
 */
public class InstallTask extends DefaultTask {

    private HelmService helmService;

    public static final String GROUP = "Kimber";
    public static final String NAME = "install";
    public static final String DESCRIPTION = "Installs the helm chart.";

    /**
     * Default constructor.
     */
    public InstallTask() {
        helmService = FactoryUtil.get(HelmService.class);
    }

    /**
     * Action to run when the task is executed.
     */
    @TaskAction
    public void action() {
        helmService.install(getProject());
    }

}