package com.sidneysimmons.kimber.task;

import com.sidneysimmons.kimber.service.KubectlService;
import com.sidneysimmons.kimber.service.PropertyOverrideService;
import com.sidneysimmons.kimber.service.TaskAvoidanceService;
import com.sidneysimmons.kimber.util.FactoryUtil;
import com.sidneysimmons.kimber.util.ProjectUtil;
import java.io.File;
import org.gradle.api.DefaultTask;
import org.gradle.api.Project;
import org.gradle.api.tasks.InputDirectory;
import org.gradle.api.tasks.Optional;
import org.gradle.api.tasks.OutputFile;
import org.gradle.api.tasks.TaskAction;

/**
 * Task used to create a kubernetes config map from any property overrides specific to this project.
 */
public class CreatePropertyOverridesConfigMapTask extends DefaultTask {

    private KubectlService kubectlService;
    private PropertyOverrideService propertyOverrideService;
    private TaskAvoidanceService taskAvoidanceService;

    public static final String GROUP = "Kimber";
    public static final String NAME = "createPropertyOverridesConfigMap";
    public static final String DESCRIPTION = "Creates the property overrides config map.";

    /**
     * Default constructor.
     */
    public CreatePropertyOverridesConfigMapTask() {
        kubectlService = FactoryUtil.get(KubectlService.class);
        propertyOverrideService = FactoryUtil.get(PropertyOverrideService.class);
        taskAvoidanceService = FactoryUtil.get(TaskAvoidanceService.class);
    }

    /**
     * Action to run when the task is executed.
     */
    @TaskAction
    public void action() {
        // Delete the existing property overrides config map
        String configMapName = ProjectUtil.getFullHierarchyProjectName(getProject()) + "-property-overrides";
        kubectlService.deleteConfigMap(configMapName);

        // Create the new property overrides config map
        Project project = getProject();
        File propertyOverridesDirectory = propertyOverrideService.getProjectDirectory(project);
        if (propertyOverridesDirectory.exists()) {
            kubectlService.createConfigMap(configMapName, propertyOverridesDirectory);
        }

        // Save the task timestamp
        taskAvoidanceService.saveTaskTimestamp(project, NAME);
    }

    /**
     * Define this project's property overrides directory as an input to this task.
     * 
     * @return the property overrides directory if it exists, null otherwise
     */
    @Optional
    @InputDirectory
    public File getPropertyOverridesDirectory() {
        File propertyOverridesDirectory = propertyOverrideService.getProjectDirectory(getProject());
        if (propertyOverridesDirectory.exists()) {
            return propertyOverridesDirectory;
        } else {
            return null;
        }
    }

    /**
     * Define this task's timestamp as the output for this task.
     * 
     * @return the task's timestamp
     */
    @OutputFile
    public File getTaskTimestamp() {
        return taskAvoidanceService.getTaskTimestamp(getProject(), NAME);
    }

}