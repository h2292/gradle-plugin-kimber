package com.sidneysimmons.kimber.task;

import com.sidneysimmons.kimber.service.DockerService;
import com.sidneysimmons.kimber.service.TaskAvoidanceService;
import com.sidneysimmons.kimber.util.FactoryUtil;
import java.io.File;
import org.gradle.api.DefaultTask;
import org.gradle.api.Project;
import org.gradle.api.tasks.InputDirectory;
import org.gradle.api.tasks.InputFile;
import org.gradle.api.tasks.Optional;
import org.gradle.api.tasks.OutputFile;
import org.gradle.api.tasks.TaskAction;

/**
 * Task used to build the docker image.
 */
public class BuildImageTask extends DefaultTask {

    private DockerService dockerService;
    private TaskAvoidanceService taskAvoidanceService;

    public static final String GROUP = "Kimber";
    public static final String NAME = "buildImage";
    public static final String DESCRIPTION = "Builds the docker image.";

    /**
     * Default constructor.
     */
    public BuildImageTask() {
        dockerService = FactoryUtil.get(DockerService.class);
        taskAvoidanceService = FactoryUtil.get(TaskAvoidanceService.class);
    }

    /**
     * Action to run when the task is executed. Build the image via docker if a dockerfile exists -
     * otherwise build the image via pack. Note that you can disable image building entirely via the
     * extension.
     */
    @TaskAction
    public void action() {
        // Build the image
        Project project = getProject();
        if (dockerService.getDockerFile(project).exists()) {
            dockerService.buildImage(project);
        }

        // Save the task timestamp
        taskAvoidanceService.saveTaskTimestamp(project, NAME);
    }

    /**
     * Define the project's dockerfile as an input to this task.
     * 
     * @return the dockerfile if it exists, null otherwise
     */
    @Optional
    @InputFile
    public File getDockerFile() {
        File dockerFile = dockerService.getDockerFile(getProject());
        if (dockerFile.exists()) {
            return dockerFile;
        } else {
            return null;
        }
    }

    /**
     * Define the "${projectDir}/build/libs" directory as an input to this task. This is where the
     * application jar is built. This is an optional input because someone might want to build an image
     * from a dockerfile in the repo that doesn't use anything built in the "/libs" directory.
     * 
     * @return the libs directory if it exists, null otherwise
     */
    @Optional
    @InputDirectory
    public File getLibsDirectory() {
        File libsDirectory = new File(getProject().getBuildDir() + "/libs");
        if (libsDirectory.exists()) {
            return libsDirectory;
        } else {
            return null;
        }
    }

    /**
     * Define this task's timestamp as the output for this task.
     * 
     * @return the task's timestamp
     */
    @OutputFile
    public File getTaskTimestamp() {
        return taskAvoidanceService.getTaskTimestamp(getProject(), NAME);
    }

}