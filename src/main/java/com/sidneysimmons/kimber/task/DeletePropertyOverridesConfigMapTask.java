package com.sidneysimmons.kimber.task;

import com.sidneysimmons.kimber.service.KubectlService;
import com.sidneysimmons.kimber.service.TaskAvoidanceService;
import com.sidneysimmons.kimber.util.FactoryUtil;
import com.sidneysimmons.kimber.util.ProjectUtil;
import org.gradle.api.DefaultTask;
import org.gradle.api.tasks.TaskAction;

/**
 * Task used to delete the kubernetes config map that contains any property overrides specific to
 * this project.
 */
public class DeletePropertyOverridesConfigMapTask extends DefaultTask {

    private KubectlService kubectlService;
    private TaskAvoidanceService taskAvoidanceService;

    public static final String GROUP = "Kimber";
    public static final String NAME = "deletePropertyOverridesConfigMap";
    public static final String DESCRIPTION = "Deletes the property overrides config map.";

    /**
     * Default constructor.
     */
    public DeletePropertyOverridesConfigMapTask() {
        kubectlService = FactoryUtil.get(KubectlService.class);
        taskAvoidanceService = FactoryUtil.get(TaskAvoidanceService.class);
    }

    /**
     * Action to run when the task is executed.
     */
    @TaskAction
    public void action() {
        // Delete the existing property overrides config map
        String configMapName = ProjectUtil.getFullHierarchyProjectName(getProject()) + "-property-overrides";
        kubectlService.deleteConfigMap(configMapName);

        // Delete the creation task's timestamp to make sure it runs the next time it's called even if data
        // hasn't changed
        taskAvoidanceService.deleteTaskTimestamp(getProject(), CreatePropertyOverridesConfigMapTask.NAME);
    }

}