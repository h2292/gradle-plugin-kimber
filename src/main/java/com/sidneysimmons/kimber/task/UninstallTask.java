package com.sidneysimmons.kimber.task;

import com.sidneysimmons.kimber.service.HelmService;
import com.sidneysimmons.kimber.util.FactoryUtil;
import org.gradle.api.DefaultTask;
import org.gradle.api.tasks.TaskAction;

/**
 * Task used to uninstall the helm chart.
 */
public class UninstallTask extends DefaultTask {

    private HelmService helmService;

    public static final String GROUP = "Kimber";
    public static final String NAME = "uninstall";
    public static final String DESCRIPTION = "Uninstalls the helm chart.";

    /**
     * Default constructor.
     */
    public UninstallTask() {
        helmService = FactoryUtil.get(HelmService.class);
    }

    /**
     * Action to run when the task is executed.
     */
    @TaskAction
    public void action() {
        helmService.uninstall(getProject());
    }

}