package com.sidneysimmons.kimber.service;

import com.sidneysimmons.kimber.util.ProjectUtil;
import java.io.File;
import org.gradle.api.Project;

/**
 * Service class for working with this project's property overrides.
 */
public class PropertyOverrideService {

    /**
     * Get the project directory.
     * 
     * Example: "~/.kimber/gradle-plugin-kimber"
     * 
     * @param project the project
     * @return the project directory
     */
    public File getProjectDirectory(Project project) {
        return new File(System.getProperty("user.home") + "/.kimber/" + ProjectUtil.getFullHierarchyProjectPath(project));
    }

    /**
     * Get the helm global file.
     * 
     * Example: "~/.kimber/helm-global.yaml"
     * 
     * @return the helm global file
     */
    public File getHelmGlobalFile() {
        return new File(System.getProperty("user.home") + "/.kimber/helm-global.yaml");
    }

    /**
     * Get the helm project file.
     * 
     * Example: "~/.kimber/gradle-plugin-kimber/helm-project.yaml"
     * 
     * @param project the project
     * @return the helm project file
     */
    public File getHelmProjectFile(Project project) {
        return new File(System.getProperty("user.home") + "/.kimber/" + ProjectUtil.getFullHierarchyProjectPath(project) + "/helm-project.yaml");
    }

}
