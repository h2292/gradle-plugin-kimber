package com.sidneysimmons.kimber.service;

import com.sidneysimmons.kimber.util.FactoryUtil;
import com.sidneysimmons.kimber.util.ProjectUtil;
import java.io.File;
import java.util.Arrays;
import java.util.List;
import org.gradle.api.Project;

/**
 * Service class for working with the "docker" CLI tool.
 */
public class DockerService {

    private ProcessService processService;

    /**
     * Default constructor.
     */
    public DockerService() {
        processService = FactoryUtil.get(ProcessService.class);
    }

    /**
     * Print the contexts.
     */
    public void printContexts() {
        processService.executeAndLog(Arrays.asList("docker", "context", "ls"), null);
    }

    /**
     * Build the image.
     * 
     * @param project the project
     */
    public void buildImage(Project project) {
        List<String> command = Arrays.asList("docker", "build", "-t", "kimber/" + ProjectUtil.getFullHierarchyProjectName(project) + ":local", "-f",
                project.getProjectDir().getAbsolutePath() + "/Dockerfile", project.getProjectDir().getAbsolutePath());
        processService.executeAndLog(command, null);
    }

    /**
     * Get the dockerfile. Note that it might not exist depending on the project.
     * 
     * @param project the project
     * @return the dockerfile
     */
    public File getDockerFile(Project project) {
        return new File(project.getProjectDir().getAbsolutePath() + "/Dockerfile");
    }

}
