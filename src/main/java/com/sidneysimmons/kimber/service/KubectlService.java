package com.sidneysimmons.kimber.service;

import com.sidneysimmons.kimber.util.FactoryUtil;
import java.io.File;
import java.util.Arrays;

/**
 * Service class for working with the "kubectl" CLI tool.
 */
public class KubectlService {

    private ProcessService processService;

    /**
     * Default constructor.
     */
    public KubectlService() {
        processService = FactoryUtil.get(ProcessService.class);
    }

    /**
     * Get the kubectl version.
     * 
     * @return the kubectl version
     */
    public String getKubectlVersion() {
        return processService.executeAndCapture(Arrays.asList("kubectl", "version"), null).get(0);
    }

    /**
     * Print the contexts.
     */
    public void printContexts() {
        processService.executeAndLog(Arrays.asList("kubectl", "config", "get-contexts"), null);
    }

    /**
     * Delete a config map with the given name. Note that this returns successful even if the config map
     * doesn't exist.
     * 
     * @param configMapName the config map name
     */
    public void deleteConfigMap(String configMapName) {
        processService.executeAndLog(Arrays.asList("kubectl", "delete", "configmap", configMapName, "--ignore-not-found"), null);
    }

    /**
     * Create a config map with the given name and source. The source may be a directory or a file.
     * 
     * @param configMapName the config map name
     * @param source the source
     */
    public void createConfigMap(String configMapName, File source) {
        processService.executeAndLog(Arrays.asList("kubectl", "create", "configmap", configMapName, "--from-file=" + source.getAbsolutePath()), null);
    }

}
