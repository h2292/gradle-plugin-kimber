package com.sidneysimmons.kimber.service;

import com.sidneysimmons.kimber.exception.TaskTimestampException;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import org.gradle.api.Project;

/**
 * Service class for working with gradle's task avoidance and input/output functionality. Task
 * timestamps can be used as task outputs to help gradle know the task is UP-TO-DATE and doesn't
 * need to run again until something changes. This is useful for tasks that build or deploy docker
 * images when the task output isn't a simple file that can be found on the file system.
 */
public class TaskAvoidanceService {

    /**
     * Returns the task timestamp file for the given task.
     * 
     * @param project the project
     * @param taskName the task name
     * @return the task timestamp file
     */
    public File getTaskTimestamp(Project project, String taskName) {
        File file = new File(project.getBuildDir() + "/tmp/outputs/task-" + taskName + ".txt");
        file.getParentFile().mkdirs();
        return file;
    }

    /**
     * Saves the task timestamp file for the given task.
     * 
     * @param project the project
     * @param taskName the task name
     */
    public void saveTaskTimestamp(Project project, String taskName) {
        File file = getTaskTimestamp(project, taskName);
        try {
            Files.write(file.toPath(), Long.toString(System.currentTimeMillis()).getBytes());
        } catch (IOException e) {
            throw new TaskTimestampException("Cannot save task timestamp. Task name = " + taskName, e);
        }
    }

    /**
     * Deletes the task timestamp file for the given task.
     * 
     * @param project the project
     * @param taskName the task name
     */
    public void deleteTaskTimestamp(Project project, String taskName) {
        File file = getTaskTimestamp(project, taskName);
        if (file.exists()) {
            file.delete();
        }
    }

}
