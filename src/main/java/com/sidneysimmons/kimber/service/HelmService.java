package com.sidneysimmons.kimber.service;

import com.sidneysimmons.kimber.util.FactoryUtil;
import com.sidneysimmons.kimber.util.ProjectUtil;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.gradle.api.Project;

/**
 * Service class for working with the "helm" CLI tool.
 */
public class HelmService {

    private PropertyOverrideService propertyOverrideService;
    private ProcessService processService;

    /**
     * Default constructor.
     */
    public HelmService() {
        propertyOverrideService = FactoryUtil.get(PropertyOverrideService.class);
        processService = FactoryUtil.get(ProcessService.class);
    }

    /**
     * Get the helm version.
     * 
     * @return the helm version
     */
    public String getHelmVersion() {
        return processService.executeAndCapture(Arrays.asList("helm", "version"), null).get(0);
    }

    /**
     * Install the helm chart. This is actually the "upgrade" command with the "--install" flag.
     * 
     * @param project the project
     */
    public void install(Project project) {
        // Get the chart details
        String releaseName = ProjectUtil.getFullHierarchyProjectName(project);
        String chartDirectory = project.getProjectDir().getAbsolutePath() + "/helm/chart";
        File helmGlobalFile = propertyOverrideService.getHelmGlobalFile();
        File helmProjectFile = propertyOverrideService.getHelmProjectFile(project);

        // Build and execute the command
        List<String> command = new ArrayList<>();
        command.add("helm");
        command.add("upgrade");

        // Tell helm to install the chart if it hasn't already been installed
        command.add("--install");

        // Tell helm that if a previously passed in value override isn't passed in again (deleted the
        // helm-project.yaml file?) to reset that
        // value to the default in the chart values
        command.add("--reset-values");

        appendHelmValueFiles(command, helmGlobalFile, helmProjectFile);
        command.add(releaseName);
        command.add(chartDirectory);
        processService.executeAndLog(command, null);
    }

    /**
     * Uninstall the helm chart.
     * 
     * @param project the project
     */
    public void uninstall(Project project) {
        // Get the chart details
        String chartName = ProjectUtil.getFullHierarchyProjectName(project);

        // Build and execute the command
        processService.executeAndLog(Arrays.asList("helm", "uninstall", chartName), null);
    }

    /**
     * Append the helm value files to the given command. Only files that exist are appended. The order
     * of the files matters - files toward the end of the array take precedence.
     * 
     * @param command the command
     * @param files the helm value files
     */
    private void appendHelmValueFiles(List<String> command, File... files) {
        for (File file : files) {
            if (file.exists()) {
                command.add("-f");
                command.add(file.getAbsolutePath());
            }
        }
    }

}
