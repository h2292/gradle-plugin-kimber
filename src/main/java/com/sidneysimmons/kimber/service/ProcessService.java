package com.sidneysimmons.kimber.service;

import com.sidneysimmons.kimber.exception.ProcessExecutionException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.gradle.api.logging.Logger;
import org.gradle.api.logging.Logging;

/**
 * Service class for executing processes.
 */
public class ProcessService {

    private static final Logger log = Logging.getLogger(ProcessService.class);

    /**
     * Execute the given process command and log the output.
     * 
     * @param command the command
     * @param environmentVariables any environment variables to set on the new process (optional)
     * @throws ProcessExecutionException thrown if there is a problem executing the command
     */
    public void executeAndLog(List<String> command, Map<String, String> environmentVariables) throws ProcessExecutionException {
        // Log the command and environment variables (users must enable info logging via the "--info"
        // option)
        if (log.isInfoEnabled()) {
            log.info("Executing process: " + command);
            log.info("Environment variables: " + environmentVariables);
        }

        // Start the process
        ProcessBuilder processBuilder = new ProcessBuilder();
        processBuilder.redirectErrorStream(true);
        processBuilder.command(command);

        if (environmentVariables != null) {
            processBuilder.environment().putAll(environmentVariables);
        }

        Process process = null;
        try {
            process = processBuilder.start();
        } catch (IOException e) {
            throw new ProcessExecutionException("Cannot start the process.", e);
        }

        // Log the result
        logProcessOutput(process);

        // Wait for the process to finish
        int exitCode;
        try {
            exitCode = process.waitFor();
        } catch (InterruptedException e) {
            throw new ProcessExecutionException("Cannot wait for the process to complete.", e);
        }

        // Check the exit code
        if (exitCode != 0) {
            throw new ProcessExecutionException("Task exited with error code: " + exitCode);
        }
    }

    /**
     * Execute the given process command and capture the output.
     * 
     * @param command the command
     * @param environmentVariables any environment variables to set on the new process (optional)
     * @return the output
     * @throws ProcessExecutionException thrown if there is a problem executing the command
     */
    public List<String> executeAndCapture(List<String> command, Map<String, String> environmentVariables) throws ProcessExecutionException {
        // Log the command and environment variables (users must enable info logging via the "--info"
        // option)
        if (log.isInfoEnabled()) {
            log.info("Executing process: " + command);
            log.info("Environment variables: " + environmentVariables);
        }

        // Start the process
        ProcessBuilder processBuilder = new ProcessBuilder();
        processBuilder.redirectErrorStream(true);
        processBuilder.command(command);

        if (environmentVariables != null) {
            processBuilder.environment().putAll(environmentVariables);
        }

        Process process = null;
        try {
            process = processBuilder.start();
        } catch (IOException e) {
            throw new ProcessExecutionException("Cannot start the process.", e);
        }

        // Capture the result
        List<String> output = captureProcessOutput(process);

        // Wait for the process to finish
        int exitCode;
        try {
            exitCode = process.waitFor();
        } catch (InterruptedException e) {
            throw new ProcessExecutionException("Cannot wait for the process to complete.", e);
        }

        // Check the exit code
        if (exitCode != 0) {
            throw new ProcessExecutionException("Task exited with error code: " + exitCode);
        }

        // Return the output
        return output;
    }

    /**
     * Log the output from the given process.
     * 
     * @param process the process
     * @throws ProcessExecutionException thrown if there is a problem executing the command
     */
    private void logProcessOutput(Process process) throws ProcessExecutionException {
        BufferedReader inputReader = null;
        InputStreamReader inputStreamReader = null;
        try {
            inputStreamReader = new InputStreamReader(process.getInputStream());
            inputReader = new BufferedReader(inputStreamReader);
            String line = null;
            while ((line = inputReader.readLine()) != null) {
                line = line.trim();
                if (line.length() > 0) {
                    // Log at the lifecycle level so it's displayed to the user
                    log.lifecycle(line);
                }
            }
        } catch (IOException e) {
            throw new ProcessExecutionException("Cannot log the process.", e);
        } finally {
            if (inputReader != null) {
                try {
                    inputReader.close();
                } catch (IOException e) {
                    // We can ignore this
                }
            }
            if (inputStreamReader != null) {
                try {
                    inputStreamReader.close();
                } catch (IOException e) {
                    // We can ignore this
                }
            }
        }
    }

    /**
     * Capture the output from the given process.
     * 
     * @param process the process
     * @return the output
     * @throws ProcessExecutionException thrown if there is a problem executing the command
     */
    private List<String> captureProcessOutput(Process process) throws ProcessExecutionException {
        List<String> output = new ArrayList<>();
        BufferedReader inputReader = null;
        InputStreamReader inputStreamReader = null;
        try {
            inputStreamReader = new InputStreamReader(process.getInputStream());
            inputReader = new BufferedReader(inputStreamReader);
            String line = null;
            while ((line = inputReader.readLine()) != null) {
                line = line.trim();
                if (line.length() > 0) {
                    // Log at the info level so it isn't automatically displayed to the user but it's available in case
                    // someone needs to
                    // troubleshoot a failed process
                    if (log.isInfoEnabled()) {
                        log.info(line);
                    }
                    output.add(line);
                }
            }
        } catch (IOException e) {
            throw new ProcessExecutionException("Cannot log the process.", e);
        } finally {
            if (inputReader != null) {
                try {
                    inputReader.close();
                } catch (IOException e) {
                    // We can ignore this
                }
            }
            if (inputStreamReader != null) {
                try {
                    inputStreamReader.close();
                } catch (IOException e) {
                    // We can ignore this
                }
            }
        }
        return output;
    }

}
