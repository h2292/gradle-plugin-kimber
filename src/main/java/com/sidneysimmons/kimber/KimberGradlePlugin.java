package com.sidneysimmons.kimber;

import com.sidneysimmons.kimber.extension.KimberGradlePluginExtension;
import com.sidneysimmons.kimber.task.BuildImageTask;
import com.sidneysimmons.kimber.task.CreatePropertyOverridesConfigMapTask;
import com.sidneysimmons.kimber.task.DeletePropertyOverridesConfigMapTask;
import com.sidneysimmons.kimber.task.InstallTask;
import com.sidneysimmons.kimber.task.UninstallTask;
import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.Task;
import org.gradle.api.tasks.TaskContainer;

/**
 * Kimber gradle plugin main class.
 */
public class KimberGradlePlugin implements Plugin<Project> {

    @Override
    public void apply(Project project) {
        // Create the plugin's extension so users can configure the plugin
        project.getExtensions().create(KimberGradlePluginExtension.NAME, KimberGradlePluginExtension.class, project);

        // Create task to build the docker image
        TaskContainer taskContainer = project.getTasks();
        taskContainer.register(BuildImageTask.NAME, BuildImageTask.class, task -> {
            task.setGroup(BuildImageTask.GROUP);
            task.setDescription(BuildImageTask.DESCRIPTION);
            Task buildTask = task.getProject().getTasks().findByName("build");
            if (buildTask != null) {
                task.dependsOn(buildTask);
            }
        });

        // Create task to create a kubernetes config map from any property overrides specific to this
        // project
        taskContainer.register(CreatePropertyOverridesConfigMapTask.NAME, CreatePropertyOverridesConfigMapTask.class, task -> {
            task.setGroup(CreatePropertyOverridesConfigMapTask.GROUP);
            task.setDescription(CreatePropertyOverridesConfigMapTask.DESCRIPTION);
        });

        // Create task to delete the kubernetes config map that contains any property overrides specific to
        // this project
        taskContainer.register(DeletePropertyOverridesConfigMapTask.NAME, DeletePropertyOverridesConfigMapTask.class, task -> {
            task.setGroup(DeletePropertyOverridesConfigMapTask.GROUP);
            task.setDescription(DeletePropertyOverridesConfigMapTask.DESCRIPTION);
        });

        // Create task to install the helm chart
        taskContainer.register(InstallTask.NAME, InstallTask.class, task -> {
            task.setGroup(InstallTask.GROUP);
            task.setDescription(InstallTask.DESCRIPTION);
            task.dependsOn(BuildImageTask.NAME, CreatePropertyOverridesConfigMapTask.NAME);
        });

        // Create task to uninstall the helm chart
        taskContainer.register(UninstallTask.NAME, UninstallTask.class, task -> {
            task.setGroup(UninstallTask.GROUP);
            task.setDescription(UninstallTask.DESCRIPTION);
            task.dependsOn(DeletePropertyOverridesConfigMapTask.NAME);
        });
    }

}
