package com.sidneysimmons.kimber.util;

import com.sidneysimmons.kimber.exception.FactoryException;
import java.util.HashMap;
import java.util.Map;

/**
 * Utility class for creating and storing shared/singleton objects.
 */
public class FactoryUtil {

    private static Map<Class<?>, Object> objects = new HashMap<>();

    /**
     * Default constructor.
     */
    private FactoryUtil() {
        // Keep private because this is a utility class
    }

    /**
     * Get an object for the given class. The object will be created if it doesn't exist. All requested
     * objects must have a constructor with no arguments.
     * 
     * @param <T> the type of class to get/create
     * @param clazz the class
     * @return the object
     */
    @SuppressWarnings("unchecked")
    public static synchronized <T> T get(Class<T> clazz) {
        if (!objects.containsKey(clazz)) {
            try {
                objects.put(clazz, clazz.newInstance());
            } catch (InstantiationException | IllegalAccessException e) {
                throw new FactoryException("Factory cannot create an object. class = " + clazz, e);
            }
        }
        return (T) objects.get(clazz);
    }

}
