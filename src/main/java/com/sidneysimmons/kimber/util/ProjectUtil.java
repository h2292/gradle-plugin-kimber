package com.sidneysimmons.kimber.util;

import com.sidneysimmons.kimber.extension.KimberGradlePluginExtension;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.gradle.api.Project;

/**
 * Utility class for working with the gradle project / sub-projects.
 */
public class ProjectUtil {

    /**
     * Default constructor.
     */
    private ProjectUtil() {
        // Keep private because this is a utility class
    }

    /**
     * Get our plugin's extension off of the project.
     * 
     * @param project the project
     * @return our plugin's extension
     */
    public static KimberGradlePluginExtension getExtension(Project project) {
        return (KimberGradlePluginExtension) project.getExtensions().getByName(KimberGradlePluginExtension.NAME);
    }

    /**
     * Get the full hierarchy project name. If the main project is called "abc" and it has a subproject
     * called "def" the value returned will be "abc-def".
     * 
     * @param project the project
     * @return the full hierarchy project name
     */
    public static String getFullHierarchyProjectName(Project project) {
        List<String> projectNames = getFullHierarchyProjectNames(project);
        return projectNames.stream().collect(Collectors.joining("-"));
    }

    /**
     * Get the full hierarchy project path.If the main project is called "abc" and it has a subproject
     * called "def" the value returned will be "abc/def".
     * 
     * @param project the project
     * @return the full hierarchy project path
     */
    public static String getFullHierarchyProjectPath(Project project) {
        List<String> projectNames = getFullHierarchyProjectNames(project);
        return projectNames.stream().collect(Collectors.joining("/"));
    }

    /**
     * Turn the project hierarchy into a list of project names where the first name in the list is the
     * root project.
     * 
     * @param project the project
     * @return a list of project names
     */
    private static List<String> getFullHierarchyProjectNames(Project project) {
        // Start with the current project name
        List<String> projectNames = new ArrayList<>();
        projectNames.add(project.getName());

        // Traverse up through the project hierarchy
        Project parentProject = project.getParent();
        while (parentProject != null) {
            projectNames.add(0, parentProject.getName());
            parentProject = parentProject.getParent();
        }
        return projectNames;
    }

}
